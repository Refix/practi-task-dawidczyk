package com.dawidczyk.practitask.service;

import com.dawidczyk.practitask.entity.BookEntity;
import com.dawidczyk.practitask.exception.BookAlreadyExistException;
import com.dawidczyk.practitask.exception.BookNotFoundException;
import com.dawidczyk.practitask.model.Book;
import com.dawidczyk.practitask.repository.BookRepository;
import lombok.Value;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class BookServiceTest {

    private static final Fixtures fixtures = new Fixtures();

    @Mock
    private BookRepository bookRepository;

    @InjectMocks
    private BookService bookService;

    @Test
    public void shouldReturnCollectionOfBooks() {
        //given
        given(bookRepository.findAll()).willReturn(fixtures.entityBooks);
        //when
        List<Book> books = bookService.getAllBooks();
        //then
        assertThat(books).isEqualTo(fixtures.responseBooks);
    }

    @Test
    public void shouldModifyBook() {
        //given
        given(bookRepository.findByIsbn(fixtures.exampleIsbn)).willReturn(Optional.of(fixtures.bookEntity1));
        //when
        bookService.modifyExistingBookData(fixtures.exampleIsbn, fixtures.modifiedBook1);
        //then
        verify(bookRepository).save(fixtures.modifiedBookEntity1);
    }

    @Test
    public void shouldThrowBookNotFoundExceptionForModify() {
        //given
        given(bookRepository.findByIsbn(fixtures.exampleIsbn)).willReturn(Optional.empty());
        //when
        final Throwable throwable = catchThrowable(() -> bookService.modifyExistingBookData(fixtures.exampleIsbn, fixtures.book1));
        //then
        assertThat(throwable).isInstanceOf(BookNotFoundException.class);
    }

    @Test
    public void shouldReturnBook() {
        //given
        given(bookRepository.findByIsbn(fixtures.exampleIsbn)).willReturn(Optional.of(fixtures.bookEntity1));
        //when
        final Book book = bookService.getBook(fixtures.exampleIsbn);
        //then
        assertThat(book).isEqualTo(fixtures.book1);
    }

    @Test
    public void shouldThrowBookNotFoundExceptionForGet() {
        //given
        given(bookRepository.findByIsbn(fixtures.exampleIsbn)).willReturn(Optional.empty());
        //when
        final Throwable throwable = catchThrowable(() -> bookService.getBook(fixtures.exampleIsbn));
        //then
        assertThat(throwable).isInstanceOf(BookNotFoundException.class);
    }

    @Test
    public void shouldAddBook() {
        //given
        given(bookRepository.existsByIsbn(fixtures.exampleIsbn)).willReturn(false);
        //when
        bookService.addBook(fixtures.book1);
        //then
        verify(bookRepository).save(fixtures.bookEntity1);
    }

    @Test
    public void shouldThrowBookAlreadyExistExceptionForAddBook() {
        //given
        given(bookRepository.existsByIsbn(fixtures.exampleIsbn)).willReturn(true);
        //when
        final Throwable throwable = catchThrowable(() -> bookService.addBook(fixtures.book1));
        //then
        assertThat(throwable).isInstanceOf(BookAlreadyExistException.class);
        verify(bookRepository, never()).save(fixtures.bookEntity1);
    }

    @Test
    public void shouldDeleteBook() {
        //given
        given(bookRepository.existsByIsbn(fixtures.exampleIsbn)).willReturn(true);
        //when
        bookService.deleteBookByIsbn(fixtures.exampleIsbn);
        //then
        verify(bookRepository).deleteByIsbn(fixtures.exampleIsbn);
    }

    @Test
    public void shouldThrowBookNotFoundExceptionForDeleteBook() {
        //given
        given(bookRepository.existsByIsbn(fixtures.exampleIsbn)).willReturn(false);
        //when
        final Throwable throwable = catchThrowable(() -> bookService.deleteBookByIsbn(fixtures.exampleIsbn));
        //then
        assertThat(throwable).isInstanceOf(BookNotFoundException.class);
        verify(bookRepository, never()).delete(fixtures.bookEntity1);
    }

    @Value
    public static class Fixtures {
        String exampleIsbn = "9781473637467";

        Book book1 = Book.builder()
                .title("Factfulness")
                .author("Hans Rosling")
                .isbn(exampleIsbn)
                .numberOfPages(342)
                .rating(BigDecimal.valueOf(4.5))
                .build();

        Book modifiedBook1 = Book.builder()
                .title("Factfulness")
                .author("Hans")
                .isbn(exampleIsbn)
                .numberOfPages(342)
                .rating(BigDecimal.valueOf(2.5))
                .build();

        Book book2 = Book.builder()
                .title("Dziady")
                .author("Adam Mickiewicz")
                .isbn("9781471237467")
                .numberOfPages(342)
                .rating(BigDecimal.valueOf(4.5))
                .build();

        List<Book> responseBooks = List.of(book1, book2);

        BookEntity bookEntity1 = BookEntity.builder()
                .title("Factfulness")
                .author("Hans Rosling")
                .isbn(exampleIsbn)
                .numberOfPages(342)
                .rating(4.5)
                .build();

        BookEntity modifiedBookEntity1 = BookEntity.builder()
                .title("Factfulness")
                .author("Hans")
                .isbn(exampleIsbn)
                .numberOfPages(342)
                .rating(2.5)
                .build();

        BookEntity bookEntity2 = BookEntity.builder()
                .title("Dziady")
                .author("Adam Mickiewicz")
                .isbn("9781471237467")
                .numberOfPages(342)
                .rating(4.5)
                .build();

        List<BookEntity> entityBooks = List.of(bookEntity1, bookEntity2);
    }
}