package com.dawidczyk.practitask.controller;

import com.dawidczyk.practitask.exception.BookNotFoundException;
import com.dawidczyk.practitask.model.Book;
import com.dawidczyk.practitask.service.BookService;
import lombok.Value;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.List;

import static java.lang.String.format;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest({BookController.class, ErrorController.class})
public class BookControllerTest {

    private static final Fixtures fixtures = new Fixtures();

    @MockBean
    private BookService bookService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getAllShouldReturnBooks() throws Exception {
        //given
        given(bookService.getAllBooks()).willReturn(List.of(fixtures.book1));

        //when
        mockMvc.perform(get("/api/books"))
                //then
                .andExpect(status().isOk())
                .andExpect(content().json(fixtures.booksResponse));
    }

    @Test
    public void shouldReturnBook() throws Exception {
        //given
        given(bookService.getBook(fixtures.exampleIsbn)).willReturn(fixtures.book1);

        //when
        mockMvc.perform(get(format("/api/books/%s", fixtures.exampleIsbn)))
                //then
                .andExpect(status().isOk())
                .andExpect(content().json(fixtures.bookResponse));
    }

    @Test
    public void shouldDeleteBook() throws Exception {
        //when
        mockMvc.perform(delete(format("/api/books/%s", fixtures.exampleIsbn)))
                //then
                .andExpect(status().isNoContent());
        verify(bookService).deleteBookByIsbn(fixtures.exampleIsbn);
    }

    @Test
    public void shouldAddBook() throws Exception {
        //when
        mockMvc.perform(post("/api/books")
                .contentType(APPLICATION_JSON)
                .content(fixtures.newBookRequest))
                //then
                .andExpect(status().isCreated());
        verify(bookService).addBook(fixtures.book1);
    }

    @Test
    public void shouldModifyBook() throws Exception {
        //when
        mockMvc.perform(put(format("/api/books/%s", fixtures.exampleIsbn))
                .contentType(APPLICATION_JSON)
                .content(fixtures.bookModifiedRequest))
                //then
                .andExpect(status().isNoContent());
        verify(bookService).modifyExistingBookData(fixtures.exampleIsbn, fixtures.modifiedBook1);
    }

    @Test
    public void shouldReturn404ForBookNotFound() throws Exception {
        //given
        given(bookService.getBook(fixtures.exampleIsbn)).willThrow(BookNotFoundException.class);

        //when
        mockMvc.perform(get(format("/api/books/%s", fixtures.exampleIsbn)))
                //then
                .andExpect(status().isNotFound());
    }

    @Value
    public static class Fixtures {
        String exampleIsbn = "9781473637467";

        Book book1 = Book.builder()
                .title("Factfulness")
                .author("Hans Rosling")
                .isbn(exampleIsbn)
                .numberOfPages(342)
                .rating(BigDecimal.valueOf(4.5))
                .build();

        Book modifiedBook1 = Book.builder()
                .title("Facts")
                .author("Hans")
                .isbn(exampleIsbn)
                .numberOfPages(342)
                .rating(BigDecimal.valueOf(4.5))
                .build();

        String booksResponse = "[\n" +
                "    {\n" +
                "        \"title\": \"Factfulness\",\n" +
                "        \"author\": \"Hans Rosling\",\n" +
                "        \"isbn\": \"9781473637467\",\n" +
                "        \"numberOfPages\": 342,\n" +
                "        \"rating\": 4.5\n" +
                "    }\n" +
                "]";

        String bookResponse = "{\n" +
                "        \"title\": \"Factfulness\",\n" +
                "        \"author\": \"Hans Rosling\",\n" +
                "        \"isbn\": \"9781473637467\",\n" +
                "        \"numberOfPages\": 342,\n" +
                "        \"rating\": 4.5\n" +
                "    }";

        String newBookRequest = "{\n" +
                "        \"title\": \"Factfulness\",\n" +
                "        \"author\": \"Hans Rosling\",\n" +
                "        \"isbn\": \"9781473637467\",\n" +
                "        \"numberOfPages\": 342,\n" +
                "        \"rating\": 4.5\n" +
                "    }";

        String bookModifiedRequest = "{\n" +
                "        \"title\": \"Facts\",\n" +
                "        \"author\": \"Hans\",\n" +
                "        \"isbn\": \"9781473637467\",\n" +
                "        \"numberOfPages\": 342,\n" +
                "        \"rating\": 4.5\n" +
                "    }";

    }

}