package com.dawidczyk.practitask.repository;

import com.dawidczyk.practitask.entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BookRepository extends JpaRepository<BookEntity, Long> {

    Optional<BookEntity> findByIsbn(String isbn);

    void deleteByIsbn(String isbn);

    boolean existsByIsbn(String isbn);
}
