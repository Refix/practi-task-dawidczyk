package com.dawidczyk.practitask.controller;

import com.dawidczyk.practitask.exception.BookAlreadyExistException;
import com.dawidczyk.practitask.exception.BookNotFoundException;
import com.dawidczyk.practitask.model.ErrorResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@ControllerAdvice
public class ErrorController {

    @ExceptionHandler(BookNotFoundException.class)
    @ResponseStatus(code = NOT_FOUND)
    @ResponseBody
    public ErrorResponse handleBookNotFoundException(final BookNotFoundException exception) {
        return ErrorResponse.of(exception.getMessage());
    }

    @ExceptionHandler(BookAlreadyExistException.class)
    @ResponseStatus(code = CONFLICT)
    @ResponseBody
    public ErrorResponse handleBookAlreadyExistException(final BookAlreadyExistException exception) {
        return ErrorResponse.of(exception.getMessage());
    }

}
