package com.dawidczyk.practitask.controller;

import com.dawidczyk.practitask.model.Book;
import com.dawidczyk.practitask.service.BookService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api/books")
@AllArgsConstructor
public class BookController {

    private final BookService bookService;

    @PostMapping
    @ResponseStatus(CREATED)
    public void addBook(@Valid @RequestBody final Book book) {
        bookService.addBook(book);
    }

    @GetMapping("/{isbn}")
    @ResponseStatus(OK)
    public Book getBook(@PathVariable final String isbn) {
        return bookService.getBook(isbn);
    }

    @GetMapping
    @ResponseStatus(OK)
    public List<Book> getBooks() {
        return bookService.getAllBooks();
    }

    @PutMapping("/{isbn}")
    @ResponseStatus(NO_CONTENT)
    public void modifyExistingBookData(@PathVariable("isbn") String isbn, @Valid @RequestBody final Book book) {
        bookService.modifyExistingBookData(isbn, book);
    }

    @DeleteMapping("/{isbn}")
    @ResponseStatus(NO_CONTENT)
    public void deleteBook(@PathVariable("isbn") final String isbn) {
        bookService.deleteBookByIsbn(isbn);
    }

}
