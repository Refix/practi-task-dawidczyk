package com.dawidczyk.practitask.exception;

import static java.lang.String.format;

public class BookNotFoundException extends RuntimeException {
    public BookNotFoundException(final String isbn) {
        super(format("Book with %s does not exist.", isbn));
    }
}
