package com.dawidczyk.practitask.exception;

import static java.lang.String.format;

public class BookAlreadyExistException extends RuntimeException {
    public BookAlreadyExistException(final String isbn) {
        super(format("Book with ISBN '%s' already exist.", isbn));
    }
}
