package com.dawidczyk.practitask.service;

import com.dawidczyk.practitask.entity.BookEntity;
import com.dawidczyk.practitask.exception.BookAlreadyExistException;
import com.dawidczyk.practitask.exception.BookNotFoundException;
import com.dawidczyk.practitask.model.Book;
import com.dawidczyk.practitask.repository.BookRepository;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class BookService {

    private final BookRepository bookRepository;

    @Transactional
    public void addBook(@NonNull final Book book) {
        if (!bookRepository.existsByIsbn(book.getIsbn())) {
            bookRepository.save(BookEntity.builder()
                    .title(book.getTitle())
                    .author(book.getAuthor())
                    .isbn(book.getIsbn())
                    .numberOfPages(book.getNumberOfPages())
                    .rating(book.getRating().doubleValue())
                    .build()
            );
        } else {
            throw new BookAlreadyExistException(book.getIsbn());
        }
    }

    public Book getBook(@NonNull final String isbn) {
        return bookRepository.findByIsbn(isbn).map(it -> Book.builder()
                .title(it.getTitle())
                .author(it.getAuthor())
                .isbn(it.getIsbn())
                .isbn(it.getIsbn())
                .numberOfPages(it.getNumberOfPages())
                .rating(BigDecimal.valueOf(it.getRating()))
                .build()
        ).orElseThrow(() -> new BookNotFoundException(isbn));
    }

    //TODO Pagination is missing
    public List<Book> getAllBooks() {
        return bookRepository.findAll()
                .stream()
                .map(it -> Book.builder()
                        .title(it.getTitle())
                        .author(it.getAuthor())
                        .isbn(it.getIsbn())
                        .numberOfPages(it.getNumberOfPages())
                        .rating(BigDecimal.valueOf(it.getRating()))
                        .build()
                ).collect(toList());
    }

    @Transactional
    public void modifyExistingBookData(@NonNull final String isbn, @NonNull final Book book) {
        final var bookEntity = bookRepository.findByIsbn(isbn)
                .orElseThrow(() -> new BookNotFoundException(isbn));

        bookRepository.save(bookEntity.toBuilder()
                .title(book.getTitle())
                .author(book.getAuthor())
                .isbn(book.getIsbn())
                .numberOfPages(book.getNumberOfPages())
                .rating(book.getRating().doubleValue())
                .build());
    }

    @Transactional
    public void deleteBookByIsbn(@NonNull final String isbn) {
        if (bookRepository.existsByIsbn(isbn)) {
            bookRepository.deleteByIsbn(isbn);
        } else {
            throw new BookNotFoundException(isbn);
        }
    }

}
