package com.dawidczyk.practitask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PractiTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(PractiTaskApplication.class, args);
    }

}
