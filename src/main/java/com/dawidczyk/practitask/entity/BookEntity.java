package com.dawidczyk.practitask.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import static javax.persistence.GenerationType.IDENTITY;
import static lombok.AccessLevel.PRIVATE;

@Value
@NoArgsConstructor(force = true)
@AllArgsConstructor(access = PRIVATE)
@Builder(toBuilder = true)
@Entity
public class BookEntity {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    Long id;

    String title;
    String author;
    String isbn;
    int numberOfPages;
    double rating;

}
