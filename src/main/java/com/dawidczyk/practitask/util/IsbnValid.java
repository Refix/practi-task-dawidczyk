package com.dawidczyk.practitask.util;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = IsbnValidator.class)
@Target( {ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface IsbnValid {
    String message() default "Invalid isbn number";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
