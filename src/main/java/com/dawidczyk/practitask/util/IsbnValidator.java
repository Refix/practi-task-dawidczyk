package com.dawidczyk.practitask.util;

import org.apache.commons.validator.routines.ISBNValidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IsbnValidator implements ConstraintValidator<IsbnValid, String> {

    private static final ISBNValidator ISBN_VALIDATOR = new ISBNValidator();

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return ISBN_VALIDATOR.isValid(s);
    }

    @Override
    public void initialize(IsbnValid constraintAnnotation) { }
}
