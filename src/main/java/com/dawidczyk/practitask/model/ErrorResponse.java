package com.dawidczyk.practitask.model;

import lombok.Value;

@Value(staticConstructor = "of")
public class ErrorResponse {
    String message;
}
