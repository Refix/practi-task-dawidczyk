package com.dawidczyk.practitask.model;

import com.dawidczyk.practitask.util.IsbnValid;
import lombok.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

import static lombok.AccessLevel.PRIVATE;

@Value
@NoArgsConstructor(access = PRIVATE, force = true)
@AllArgsConstructor(access = PRIVATE)
@Builder
public class Book {

    @Size(min = 1, max = 100)
    @NonNull
    String title;

    @Size(min = 1, max = 100)
    @NonNull
    String author;

    @IsbnValid
    @NonNull
    String isbn;

    @Min(1)
    @Max(4032) // THICKEST BOOK PUBLISHED - Guinness World Record
    @NonNull
    Integer numberOfPages;

    @Min(1)
    @Max(5)
    @NonNull
    BigDecimal rating;
}
