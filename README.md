﻿How to run it in IDE:
- Install Lombok plugin
- Enable annotation processing
- Jdk 12


REMEMBER ABOUT CORRECT ISBN NUMBER!

#List all books:

GET: /api/books 
example: http://localhost:8080/api/books


#Get one book:

GET: /api/books/{isbn}
example: http://localhost:8080/api/books/9781473637467


#Add book:

POST: /api/books
example: http://localhost:8080/api/books

example json body:
{
	"title": "Factfulness",
	"author": "Hans Rosling",
	"isbn": "9781473637467",
	"numberOfPages": "342",
	"rating": "4.5"
}

#Modify book:

PUT: /api/books/{isbn}
example: http://localhost:8080/api/books/9781473637467

example json body:
{
	"title": "Factfulness",
	"author": "Hans Rosling",
	"isbn": "9781473637467",
	"numberOfPages": "342",
	"rating": "4.5"
}


#Delete book:

DELETE: /api/books/{isbn}
example: http://localhost:8080/api/books/9781473637467

